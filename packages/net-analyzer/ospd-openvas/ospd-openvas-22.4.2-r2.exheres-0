# Copyright 2019-2022 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pypi setup-py [ import=setuptools blacklist='2 3.6' has_bin=true test=pytest ] \
    systemd-service

SUMMARY="OSP server implementation to allow GVM to control OpenVAS"
DOWNLOADS+=" https://www.greenbone.net/GBCommunitySigningKey.asc -> ${PNV}-GBCommunitySigningKey.asc"

LICENCES="AGPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        app-crypt/gnupg [[ note = [ Feed Validation import ] ]]
    build+run:
        group/gvm
        user/gvm
        dev-python/defusedxml[>=0.6.0&<0.8.0][python_abis:*(-)?]
        dev-python/Deprecated[>=1.2.10&<2.0.0][python_abis:*(-)?]
        dev-python/lxml[>=4.5.2&<5.0.0][python_abis:*(-)?]
        dev-python/packaging[>=20.4&<21.0][python_abis:*(-)?]
        dev-python/paho-mqtt[>=1.5.1][python_abis:*(-)?]
        dev-python/paramiko[>=2.7.1&<3.0.0][python_abis:*(-)?]
        dev-python/psutil[>=5.5.1&<6.0.0][python_abis:*(-)?]
        dev-python/python-gnupg[>=0.4.8&<0.5.0][python_abis:*(-)?]
        dev-python/redis[>=3.5.3&<5.0.0][python_abis:*(-)?]
        !net-analyzer/ospd [[
            description = [ ospd was moved into the ospd-openvas packages ]
            resolution = uninstall-blocked-after
        ]]
"

PYTEST_PARAMS=(
    -k "not test_scan_and_result.py and not test_vts.py"
)

test_one_multibuild() {
    esandbox allow_net "unix:${TEMP%/}/pymp-*/listener-*"

    setup-py_test_one_multibuild

    esandbox disallow_net "unix:${TEMP%/}/pymp-*/listener-*"
}

install_one_multibuild() {
    setup-py_install_one_multibuild

    install_systemd_files

    insinto /etc/gvm
    doins config/ospd-openvas.conf

    # Feed Validation
    # https://greenbone.github.io/docs/latest/22.4/source-build/index.html#feed-validation
    # gnupghome should be a directory (it isn't): /etc/openvas/gnupg
    dodir /etc/openvas/gnupg
    # setup temporary gnupg directories
    export GNUPGHOME="${IMAGE}"/etc/openvas/gnupg
    edo echo -e "%Assuan%\nsocket=${TEMP}/S.gpg-agent" > "${GNUPGHOME}"/S.gpg-agent
    edo echo -e "extra-socket ${TEMP}/S.gpg-agent.extra\nbrowser-socket ${TEMP}/S.gpg-agent.browser" > "${GNUPGHOME}"/gpg-agent.conf
    edo echo -e "%Assuan%\nsocket=${TEMP}/S.gpg-agent.sshS.gpg-agent.ssh" > "${GNUPGHOME}"/S.gpg-agent.ssh
    edo echo -e "8AE4BE429B60A59B311C2E739823FAA60ED1E580:6:" > "${TEMP}"/ownertrust.txt
    # import GBCommunitySigningKey.asc
    esandbox allow_net "unix:${TEMP}/S.gpg-agent*"
    esandbox allow_net --connect "unix:${TEMP}/S.gpg-agent*"
    edo gpg --import "${FETCHEDDIR}"/${PNV}-GBCommunitySigningKey.asc
    edo gpg --import-ownertrust < "${TEMP}"/ownertrust.txt
    esandbox disallow_net --connect "unix:${TEMP}/S.gpg-agent*"
    esandbox disallow_net "unix:${TEMP}/S.gpg-agent*"
    # cleanup
    edo rm "${GNUPGHOME}"/{S.gpg-agent,S.gpg-agent.ssh,gpg-agent.conf}
    edo rmdir "${IMAGE}"/etc/openvas/gnupg/private-keys-v1.d
    unset GNUPGHOME
    # set permissions
    edo chown -R gvm:gvm "${IMAGE}"/etc/openvas/gnupg
    edo chmod 0700 "${IMAGE}"/etc/openvas/gnupg

    keepdir /var/log/gvm
    edo chown gvm:gvm "${IMAGE}"/var/log/gvm

    doman docs/ospd-openvas.8

    insinto /usr/$(exhost --target)/lib/tmpfiles.d
    hereins ${PN}.conf <<EOF
d /run/ospd 0755 root root
EOF

    # do not install test stuff into site-packages
    edo rm -rf "${IMAGE}"$(python_get_libdir)/site-packages/{setup.py,__pycache__,tests}
}

