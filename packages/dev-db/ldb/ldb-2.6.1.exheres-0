# Copyright 2011-2012 Pierre Lejeune <superheron@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require wafsamba

SUMMARY="LDAP-like embedded database"
DESCRIPTION="
ldb is a LDAP-like embedded database. It is not at all LDAP standards compliant, so if you want a
standards compliant database then please see the excellent OpenLDAP project.

What ldb does is provide a fast database with an LDAP-like API designed to be used within an
application. In some ways it can be seen as a intermediate solution between key-value pair databases
and a real LDAP database.

ldb is the database engine used in Samba4.
"
HOMEPAGE="https://${PN}.samba.org"
DOWNLOADS="mirror://samba/../${PN}/${PNV}.tar.gz"

LICENCES="LGPL-3"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS="
    lmdb [[ description = [ Enable LMDB backend support ] ]]

    ( libc: musl )
"

DEPENDENCIES="
    build+run:
        dev-db/tdb[>=1.4.7][python_abis:*(-)?]
        dev-libs/libbsd [[ note = [ automagic ] ]]
        dev-libs/popt
        dev-libs/talloc[>=2.3.4][python_abis:*(-)?]
        dev-libs/tevent[>=0.13.0][python_abis:*(-)?]
        !libc:musl? ( dev-libs/libxcrypt:= )
        lmdb? ( dev-db/lmdb[>=0.9.16] )
    build+test:
        dev-util/cmocka[>=1.1.3]
    recommendation:
        net-directory/openldap [[ description = [ LDB can use OpenLDAP as a backend ] ]]
"

DEFAULT_SRC_PREPARE_PATCHES+=(
    "${FILES}"/disable-compile-error-test.patch
)

WAF_SRC_CONFIGURE_PARAMS+=(
    --with-modulesdir=/usr/$(exhost --target)/lib
)
WAF_SRC_CONFIGURE_OPTIONS+=(
    '!lmdb --without-ldb-lmdb'
)

WAF_SRC_COMPILE_PARAMS+=( -j1 )

