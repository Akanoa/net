# Copyright 2015 Sergey Kvachonok <ravenexp@gmail.com>
# Copyright 2015 Wouter van Kesteren <woutershep@gmail.com>
# Copyright 2014, 2016 Calvin Walton <calvin.walton@kepstin.ca>
# Copyright 2010 A Frederick Christensen <fauxmight@nosocomia.com>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'foo-1.23.ebuild' from Gentoo, which is:
#     Copyright 1999-2010 Gentoo Foundation

require systemd-service

SUMMARY="User space daemon for access point and authentication servers"
DESCRIPTION="
hostapd implements IEEE 802.11 access point management, IEEE 802.1X/WPA/WPA2/EAP
Authenticators, RADIUS client, EAP server, and RADIUS authentication server.
"
HOMEPAGE="https://w1.fi/${PN}"
DOWNLOADS="https://w1.fi/releases/${PNV}.tar.gz"

LICENCES="|| ( GPL-2 BSD-3 )"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build+run:
        net-libs/libnl:3.0[>=3.2]
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:=[>=1.0.1] )
"

WORK="${WORKBASE}"/${PNV}/${PN}

DEFAULT_SRC_COMPILE_PARAMS=( V=1 all nt_password_hash )

src_prepare() {
    edo cd "${WORKBASE}"/"${PNV}"

    default

    # We install all the sample config files under /etc/hostapd/, so add
    # that prefix to them in the sample hostapd.conf
    edo sed -i -e "s:/etc/hostapd:/etc/hostapd/hostapd:g" \
        "${WORK}"/hostapd.conf
}

src_configure() {
    # Copy the defconfig to give us a working base config
    edo cp defconfig .config

    # Enable some additional features over the defconfig:
    local ITEMS=(
        # We are using libnl-3.2
        CONFIG_LIBNL32 CONFIG_VLAN_NETLINK

        # Enable support for 802.11
        CONFIG_IEEE80211AC
        CONFIG_IEEE80211AX
        CONFIG_IEEE80211N
        CONFIG_IEEE80211R
        CONFIG_IEEE80211W
        CONFIG_IEEE8021X_EAPOL

        # Enable automatic channel selection (currently nl80211 atheros only)
        CONFIG_ACS

        # Enable TLSv1.1 & 1.2, to support stronger crypto algorithms
        # This increases the openssl dep to 1.0.1
        CONFIG_TLSV11 CONFIG_TLSV12

        # Disable deprecated TKIP support
        CONFIG_NO_TKIP
    )

    for ITEM in "${ITEMS[@]}"; do
        echo "${ITEM}"=y >> .config || die "Failed to add to .config"
    done
}

src_install() {
    # This is especially messy. Yell at upstream

    insinto /etc/hostapd
    doins hostapd.conf hostapd.accept hostapd.deny hostapd.eap_user \
        hostapd.radius_clients hostapd.sim_db hostapd.wpa_psk

    dobin hostapd hostapd_cli nt_password_hash

    install_systemd_files

    doman hostapd.8 hostapd_cli.1

    emagicdocs
}

