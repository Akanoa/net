# This file specifies where Munin will look for things after you've
# run 'make' in the source directory.  Modify it to suit your needs.

# DESTDIR is empty during building, and optionally set to point to
# a shadow tree during make install.

#
# the base of the Munin installation.
# 
PREFIX     = $(DESTDIR)

# Where Munin keeps its configurations (server.conf, client.conf, ++)
CONFDIR    = ${PREFIX}/etc/munin

# Server only - where to put munin-cron
BINDIR     = $(PREFIX)/usr/bin

# Client only - where to put munin-node, munin-node-configure, and munin-run
SBINDIR    = $(PREFIX)/sbin

# Where to put text and html documentation
DOCDIR     = $(PREFIX)/usr/share/doc/munin-$(shell cat RELEASE)

# Where to put man pages
MANDIR     = $(PREFIX)/usr/share/man

# Where to put internal binaries and plugin repository
LIBDIR     = $(PREFIX)/usr/lib/munin

# Server only - Output directory
HTMLDIR    = ${PREFIX}/srv/munin
CGIDIR     = $(HTMLDIR)/cgi

# Client only - Where to put RRD files and other intenal data
DBDIR      = ${PREFIX}/var/cache/munin

# Client only - Where plugins should put their states. Must be writable by
# group "munin", and should be preserved between reboots
PLUGSTATE  = $(DBDIR)/plugin-state

# Where Munin should place its logs.
LOGDIR     = ${PREFIX}/var/log/munin

# Location of PID files and other statefiles. On the server, must be
# writable by the user "munin".
STATEDIR   = ${PREFIX}/run/munin

# The perl interpreter to use
PERL       = $(shell which perl)

# The python interpreter to use (used by some plugins)
PYTHON     = /usr/bin/env python

# A modern (posix) shell.  We're not looking for arrays, but $() and
# other modern stuff is expected.  On a posix-system the expression
# below will find the right shell.  Most Unixes released the last 10
# years are POSIX compliant enough for this to work (he said bravely).
# 
# On Linux /bin/sh, SunOS/Solaris /usr/xpg4/bin/sh or /bin/ksh
# In general: bash or ksh will work
#
GOODSH     = $(shell PATH=`getconf PATH` sh -c 'type sh | sed "s/.* //"')

# Path of bash for bash specific plugins
BASH       = /usr/bin/env bash

# Server only - Where to install the perl libraries
PERLLIB    = $(DESTDIR)$(shell $(PERL) -V:sitelib | cut -d"'" -f2)

# Client only - Install plugins for this architecture
OSTYPE     = $(shell uname | tr '[A-Z]' '[a-z]')

# How to figure out the hostname. (Only used in default configuration
# files)
HOSTNAME   = $(shell hostname)

# What is the safest way to create a tempfile.
# Default is to figure it out by testing various methods.
# Replace this with a known platform-specific method
MKTEMP     = $(shell ./test-mktemp)

# Munin version number.
VERSION    = $(shell cat RELEASE)

# User to run munin as
USER       = munin
GROUP      = munin

# Default user to run the plugins as
PLUGINUSER = nobody

# Check whether setruid functionality can be used
HASSETR = $(shell perl -e 'use Config; my @vars=("d_setruid", "d_setreuid", "d_setresuid"); foreach my $$var (@vars) { if ($$Config{$$var} eq "define") { print "1\n"; exit 0; } } print "0\n"; exit 0;' )

