# Copyright 2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=onqtam tag=v${PV} ] cmake

export_exlib_phases src_prepare

SUMMARY="Feature-rich C++11/14/17/20 single-header testing framework "
DESCRIPTION="A C++ testing framework that is by far the fastest both in
compile times (by orders of magnitude) and runtime compared to other feature-
rich alternatives. It brings the ability of compiled languages such as D /
Rust / Nim to have tests written directly in the production code thanks to a
fast, transparent and flexible test runner with a clean interface."

LICENCES="MIT"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES=""

UPSTREAM_CHANGELOG="
    https://github.com/onqtam/doctest/blob/master/CHANGELOG.md
"
UPSTREAM_DOCUMENTATION="
    https://github.com/onqtam/doctest/blob/master/doc/markdown/readme.md#reference
"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DCMAKE_DISABLE_FIND_PACKAGE_MPI:BOOL=TRUE
    -DDOCTEST_TEST_MODE="NORMAL"
    # Isn't installed anyway
    -DDOCTEST_WITH_MAIN_IN_STATIC_LIB:BOOL=FALSE
)
CMAKE_SRC_CONFIGURE_TESTS+=(
    '-DDOCTEST_WITH_TESTS:BOOL=TRUE -DDOCTEST_WITH_TESTS:BOOL=FALSE'
)

DEFAULT_SRC_TEST_PARAMS+=(
    # Skip 6 tests (from 135) which don't seem to work under sydbox
    ARGS+="-E '(asserts_used_outside_of_tests.cpp|executable_dll_and_plugin)'"
)

doctest_src_prepare() {
    cmake_src_prepare

    # Remove -Werror
    edo sed -e "/add_compiler_flags(-Werror)/d" -i scripts/cmake/common.cmake
}

