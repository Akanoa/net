# Copyright 2011 Elias Pipping <pipping@exherbo.org>
# Copyright 2015 Thomas Witt
# Distributed under the terms of the GNU General Public License v2

require pam \
    bash-completion \
    autotools [ supported_autoconf=[ 2.7 ] supported_automake=[ 1.16 ] ]

SUMMARY="User-space tools required by the in-kernel CIFS filesystem"
HOMEPAGE="https://wiki.samba.org/index.php/LinuxCIFS_utils"
DOWNLOADS="https://download.samba.org/pub/linux-cifs/${PN}/${PNV}.tar.bz2"

LICENCES="GPL-3"
SLOT="0"
MYOPTIONS="
    ads [[ description = [ Enable Active Directory support ] ]]
    pam
    systemd

    ads? ( ( providers: heimdal krb5 ) [[ number-selected = exactly-one ]] )
"

DEPENDENCIES="
    build:
        dev-python/docutils [[ note = [ rst2man for man page ] ]]
    build+run:
        sys-apps/keyutils
        sys-libs/libcap-ng
        ads? (
            dev-libs/talloc
            providers:heimdal? ( app-crypt/heimdal )
            providers:krb5? ( app-crypt/krb5 )
        )
        pam? ( sys-libs/pam )
        systemd? ( sys-apps/systemd )
        !net-fs/samba[<3.6] [[
            description = [ File collision with Samba < 3.6 ]
            resolution = upgrade-blocked-before
        ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    ROOTSBINDIR=/usr/$(exhost --target)/bin
    --with-libcap
    --with-pamdir=$(getpam_mod_dir)
    --enable-cifscreds
    --enable-man
    --enable-smbinfo
    --disable-cifsacl
    --disable-cifsidmap
    --disable-pythontools
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'ads cifsupcall'
    pam
    systemd
)

src_install() {
    default

    dobashcompletion bash-completion/smbinfo smbinfo
}

